### Git exercises.

1.	Configure your git. 
    
    1. Add user name, email. Turn on colors. Set your editor to your preferred one for example vim or gedit. Use flag -global as this config will be used in all repositories.
    2. Check if your configuration has been set. Use command git config –list
    3. Check file ~/.gitconfig if you can see your configuration

2. Clone your first repository. 
    
    1. Clone repository.
    2. Go to the main page for the repository copy in the in the right corner click on the clone button and copy link to the repository
    3. Check files in the directory. Use command ```ls -la``` to list hidden files. Use command ```gio tree -h``` to see whole directory structure.

3. Create first repository and commit first file. Use appropriate commit messages. (After every step run commands ```git status```, git log and ```git diff``` with appropriate flags)

    1.	Create directory git1 and inside initialize empty repository. 
    2.	Create empty file with name ‘resume.txt’.
    3.	Add file to staging area. 
    4.	Commit file with message “Initial commit”. 

4. Remove git repository.
    
    1.	Remove git repository from git1. Run git status and check if git repo was successfully removed.
    2.	Reinitialize repository. Check the status.
    3.	Recreate commit from previous exercise.

5. User repository from previous exercise create additional commits. Add 3 additional commits. Each time modify file, add changes to staging area and commit it.
    
    1. In first commit add your first name on the first line
    2. In second your position.
    3. In third fix first line to put your name and surname.
    4. Inspect commit history with git log command.

6. Inspect commit history in visual clients

    1. Gitk – to start program run command gitk in the root directory of the project.
    2. GitEye
   
7. Adding git ignore

    1. Add empty file with name unwanted, error.log and output.log
    2. Add .gitignore file and create entry to prevent files from previous point to be tracked. (user star pattern when appropriate)
    3. Add all files in the directory and commit them.
    4. Check if our unwanted files have been ignored.
    
8. Removing files from git.
    
    1. Create directory git2 and inside initialize empty repository.
    2. Create file A.txt. Add it to staging and commit it.
    3. Remove file. Check the status.
    4. Add removal to the staging area. And commit.

9. Removing files form staging. User repositoy from previouse exercise.

    1. Create new files A.txt and B.txt and add it to the staging area.
    2. Now you want to commit only file B.txt but both files are in staging to do so you have to remove file A.txt from staging.
    3. Commit changes. Check witch files were commited.
    4. Now commit file A and check commit history.
    
10. Basic branch operations

    1. Create new repository called branches.
    2. Create README.md file and commit with message "Initial commit"
    3. List branches.
    4. Create new branch called feature1 and switch into it.
    5. List branches.
    6. Switch back to master and List branches again.
    
11. Merging

    1. User repository from previouse exercise. Switch to branch feature1 and create, add and commit file named A.txt.
    2. Switch to master branch and merge branch feature1 in to it. 
    3. User GitEye or Gitk to check state of the repository.
    4. Remove branch feature1.
    5. Repeate the process with branch feature2 and file B.txt.
    
12. Merge conflicts

    1. User repository from previouse exercise. Create branch feature3 and add text "hello" to the file A.txt. Commit the changes.
    2. Swith to master branch. Create branch feature4 and add text "hello world" to the file A.txt. Commit the changes.
    3. Switch to master and merge branch feature4 in to it.
    4. Merge branch feature3 in to master. It will fail as there is a merge conflict.
    5. Resolve conflict. Add changed file to the staging and commit it.
    6. User GitEye or Gitk to check state of the repository. 
    
13. Set up account on bit bucket website. You can use google or facebook account.

14. Create first enpty remote repository

    1. Create new repository.
    2. Clone it to your computer.
    3. Inside repository craete new file, commit it and push.
    4. Check your repository on Bitbucket website
    
15. Upload existing local repository to bitbucket (User reposiotry from exercise 10-12)

    1. Create new repository called the same way as your local one (this is not neccery but it is good practice)
    2. Go in to local repository and set origin for master branch and push changes to remote server.
    
16. Colaboration on common repository. (With 2-3 person teams)

    1. One person should create single repository for the team.
    2. Then add other members of a team as colaborators.
    3. Each person should clone the common repo.
    4. Each person should create local feature branch called differently for example: feature1, feature2 ... . Then create single with title <firstname>_<lastname>.txt commit it, push to the remote and create pull request.
    5. Each pull request should be reviewed and merged by one of the other memebers of a team.
    


    
    
